import numpy as np


class VectorBase:

    _data = None

    @property
    def dimensions(self):
        return self._data.shape[0]

    @property
    def dim(self):
        return self.dimensions

    @property
    def x(self):
        return self._data[0]

    @x.setter
    def x(self, value):
        self._data[0] = value

    @property
    def y(self):
        return self._data[1]

    @y.setter
    def y(self, value):
        self._data[1] = value

    @property
    def z(self):
        return self._data[2]

    @z.setter
    def z(self, value):
        self._data[2] = value

    def _retrieve(self, ):


class Vector(VectorBase):

    def __init__(self, x=None, y=None, z=None):
        if (x is None) and (y is None) and (z is None):
            self._data = np.zeros(3)
            return
        if (not x is None) and (y is None) and (z is None):
            if isinstance(x, int):
                self._data = np.repeat(x, 3)
                return
            if isinstance(x, (list, np.ndarray)):
                self._data = np.array(x)
                # TODO: check the shape of x / _data


class VectorBatch(VectorBase):

    def __init__(self, x=None, y=None, z=None):
        self._data = np.zeros((3, 10))
